<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Urutan extends Model
{
    protected $fillable = ['slider_id','station_id'];
}
