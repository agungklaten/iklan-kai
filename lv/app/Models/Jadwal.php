<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Jadwal extends Model
{
    protected $fillable = ['nama_ka','no_ka','jurusan','pukul','station_id'];
}
