<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use App\User;
use Auth;
use App\Models\Admin;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function profile()
    {
      $data['menu'] = 'user';
      $data['page'] = "";
      $data['profile'] = Admin::find(Auth::user()->id);
      return view('page.profile',$data);
    }

    public function update(Request $request)
    {
      $id = Auth::user()->id;
      $uname = Auth::user()->username;
      $name = $request->name;
      $user = Admin::find($id);
      $user->name = $name;
      $user->save();

      return Response::json($user);
    }

    public function changepass(Request $request)
    {
        $id = Auth::user()->id;
        $passwd = $request->passwd;
        $oldpasswd = $request->oldpasswd;
        $user = Admin::find($id);
        if (!password_verify($oldpasswd,$user->password)) {
           $data['status'] = 'failed';
           $data['msg'] = 'Password lama yang dimasukkan salah';
           return Response::json($data);
        } else {
          $user->password = bcrypt($passwd);
          $user->save();
          $data['status'] = 'success';
          return Response::json($data);
        }
    }
}
