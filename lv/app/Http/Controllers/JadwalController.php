<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use DB;
use App\Models\Jadwal;
use Illuminate\Support\Str;
use App\Models\Station;
use Auth;

class JadwalController extends Controller
{
    public function index()
    {
    	$data['menu'] = 'jadwal';
    	$data['page'] = '';
    	$data['mobile'] = session('mobile');
      $data['stations'] = Station::get();
    	return view('page.jadwal',$data);
    }

    public function getData()
    {    	
      if (Auth::user()->station_id!=0) {
        $sql = "where station_id=".Auth::user()->station_id;
      } else {
        $sql = "";
      }
      $jadwals = DB::select("SELECT a.*,nama_stasiun from jadwals a inner join stations b on a.station_id=b.id $sql");
    	return Datatables::of($jadwals)
           ->addColumn('action', function($sl) {
            return '
            <button  class="btn btn-sm btn-info" onclick="opnPage(\''. $sl->id .'\',\''.$sl->id .'\')"><i class="fa fa-edit"></i> Ubah</button>
            <button class="btn btn-sm btn-danger" onclick="delPage(\''. $sl->id .'\')"><i class="fa fa-trash"></i> Hapus</button>
            ';
           })         
           ->rawColumns(['action'])
           ->make(true);
    }

    public function show($id)
    {
        $sl = Jadwal::find($id);
        return Response::json($sl);
    }

    public function store(Request $request)
    {
    	$nama = $request->namaka;
    	$no = $request->noka;
    	$jurusan = $request->jurusan;
    	$pukul = $request->pukul;
      $station = $request->station;
      
      $id    = $request->page_id; 
      $action = $request->action;
      if ($action=='add') {
          $jadwal = new Jadwal();
      } else {
          $jadwal = Jadwal::find($id);
      }
	    $jadwal->nama_ka = $nama;
	    $jadwal->no_ka = $no;
	    $jadwal->jurusan = $jurusan;
	    $jadwal->pukul = $pukul;
      $jadwal->station_id = $station;
	    
	    $jadwal->save();
      return Response::json($jadwal);
    }

    public function delete($id)
    {
    	$jadwal = Jadwal::find($id)->delete();
      return Response::json($jadwal);
    }
}
