<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use DB;
use App\Models\Slider;
use Illuminate\Support\Str;
use App\Models\Urutan;
use App\Models\Jadwal;
use Intervention\Image\Facades\Image;
use App\Models\Station;
use Auth;

class SliderController extends Controller
{
    public function index()
    {
    	$data['menu'] = 'slider';
    	$data['page'] = '';
    	$data['mobile'] = session('mobile');
      $data['stations'] = Station::get();
    	return view('page.slider',$data);
    }

    public function getData()
    {    	
      if (Auth::user()->station_id!=0) {
        $sql = "where station_id=".Auth::user()->station_id;
      } else {
        $sql = "";
      }
      $sliders = DB::select("SELECT a.*,nama_stasiun from sliders a inner join stations b on a.station_id=b.id $sql");
    	return Datatables::of($sliders)
           ->addColumn('action', function($sl) {
            return '
            <button  class="btn btn-sm btn-info" onclick="opnPage(\''. $sl->id .'\',\''.$sl->id .'\')"><i class="fa fa-edit"></i> Ubah</button>
            <button class="btn btn-sm btn-danger" onclick="delPage(\''. $sl->id .'\')"><i class="fa fa-trash"></i> Hapus</button>
            ';
           })
           ->editColumn('status', function($sl){
            return ($sl->status==1)?'<span class="badge bg-green">Aktif</span>':'<span class="badge bg-red">Tidak Aktif</span>';
           })    
           ->editColumn('thumbnail',function($sl){
            return "<img src='".url('/uploads/thumbs/'.$sl->thumbnail)."' alt='thumb' class='img-preview'>";
           })       
           ->rawColumns(['action','status','thumbnail'])
           ->make(true);
    }

    public function show($id)
    {
        $sl = Slider::find($id);
        return Response::json($sl);
    }

    public function showslider($slug)
    {
       $station = Station::where('stasiun_slug',$slug)->first();
       if (!$station) {
         return redirect('/');
       }

       $urutan = Urutan::where('station_id',$station->id)->first();
       $media = Slider::find($urutan->slider_id);
       if (!$media){
         $data['jadwal'] = Jadwal::where('station_id',$station->id)->get();
         $data['jadwal_durasi'] = 20;
       }
       $data['media'] = $media;
       $data['stasiun'] = $station->id;
       
       return view('page.show',$data);
    }

    public function store(Request $request)
    {
    	$title = $request->title;
    	$slug = $request->slug;
    	$tipe = $request->tipe;
      $durasi = $request->durasi;
    	$status = $request->status;
      $media = $request->media;
      $thumb = $request->thumb;
      $station = $request->station;

      $filethumb = $request->file('thumbnail');
      if ($filethumb) {
        $filename2 = $station."_".$filethumb->getClientOriginalName();
        $filepath2 = env('APP_PATH').'/uploads/thumbs/';
        if ($filepath2!=$thumb) {
          $request->file("thumbnail")->move($filepath2,$filename2);   
          $image2 = Image::make(sprintf($filepath2.'%s',$filename2));
          $image2->fit(300)->save();
        } else {
          $filename2 = $thumb;
        }
      } else {
          $filename2 = ($thumb)?$thumb:"";      
      }

      $filemedia = $request->file('file');
      if ($filemedia) {
        $filename = $station."_".$filemedia->getClientOriginalName();
        $filepath = env('APP_PATH').'/uploads/';
        if ($media!=$filename) {
          $request->file("file")->move($filepath,$filename); 
          if ($tipe=='image') {   
            $image = Image::make(sprintf($filepath.'%s',$filename));
            $image->fit(300)->save($filepath.'thumbs/'.$filename);  
            $filename2 = $filename;            
          }
        } else {
          $filename = $media;
        }
      } else {
        $filename = $media;
      }
         	
      
      $id    = $request->page_id; 
      $action = $request->action;
      if ($action=='add') {
          $slider = new Slider();
      } else {
          $slider = Slider::find($id);
      }
	    $slider->title = $title;
	    $slider->slug = $slug;
	    $slider->tipe = $tipe;
	    $slider->namafile = $filename;
      $slider->thumbnail = $filename2;
	    $slider->durasi = $durasi;
	    $slider->status = $status;
      $slider->station_id = $station;
	    $slider->save();

      if ($status==1) {
        $urutan = Urutan::where('station_id',$station)->first();
        if (!$urutan) {
          $urutan = new Urutan();
          $urutan->station_id = $station;
        }
        $urutan->slider_id = $slider->id;
        $urutan->save();
      }

      return Response::json($slider);
    }

    public function delete($id)
    {
    	$slider = Slider::find($id)->delete();
      return Response::json($slider);
    }

    public function savepicture(Request $request)
    {
        $image = $request->file('picture');
        $filename = "";
        if ($image) {
            $ext = $request->file('picture')->extension();
            $filename = Str::uuid().".$ext";
            $imgpath = env('APP_PATH').'/uploads/';
            $request->file('picture')->move($imgpath,$filename);
        }
        $data['filename'] = env('APP_URL').'/uploads/'.$filename;
        return Response::json($data);
    }

    public function detail()
    {
    	 $urutan = Urutan::first();
       $media = Slider::find($urutan->slider_id);
       if (!$media){
         $data['jadwal'] = Jadwal::get();
         $data['jadwal_durasi'] = 20;
       }
       $data['media'] = $media;
       
       return view('page.show',$data);
    }

    public function getshow($id)
    {
    	$urutan = Urutan::where('station_id',$id)->first();
      $media = Slider::where([['status',1],['id','>',$urutan->slider_id],['station_id',$id]])->first();
      if (!$media) { 
        $jadwal = Jadwal::where('station_id',$id)->get();
        $data['durasi'] = 20;
        $media = Slider::where([['status',1],['station_id',$id]])->first();
        $html = "<h1 class='jadwal'>Jadwal Kereta Api</h1>
     <table class='table table-striped table-bordered'>
        <tbody>
        <tr>
          <th class='jadwal'>Nama KA</th>
          <th class='jadwal'>No KA</th>
          <th class='jadwal'>Jurusan</th>
          <th class='jadwal'>Keberangkatan</th>
        </tr>";
        foreach($jadwal as $j) {
        $html .="<tr>
          <td class='jadwal'>".$j->nama_ka."</td>
          <td class='tengah jadwal'>". $j->no_ka ."</td>
          <td class='jadwal'>". $j->jurusan ."</td>
          <td class='tengah jadwal'>". $j->pukul ."</td>
        </tr>";
        }
        $html .="</tbody></table>";
        $data['jadwal'] = $html;
        $urutan->slider_id = $media->id;
        $urutan->save();
        $data['tipe'] = 'jadwal';        
      } else {
        $media->filename = url('/uploads/'.$media->namafile);
        $urutan->slider_id = $media->id;
        $urutan->save();
        $data['tipe'] = $media->tipe;
        $data['durasi'] = $media->durasi;
      }
      $data['media'] = $media;


      return Response::json($data);
    }
}
