<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use DB;
use App\Models\Station;
use Illuminate\Support\Str;

class StationController extends Controller
{
    public function index()
    {
    	$data['menu'] = 'master';
    	$data['page'] = 'station';
    	$data['mobile'] = session('mobile');
    	return view('page.station',$data);
    }

    public function getData()
    {    	
    	return Datatables::of(Station::query())
           ->addColumn('action', function($sl) {
            return '
            <button  class="btn btn-sm btn-info" onclick="opnPage(\''. $sl->id .'\',\''.$sl->id .'\')"><i class="fa fa-edit"></i> Ubah</button>
            <button class="btn btn-sm btn-danger" onclick="delPage(\''. $sl->id .'\')"><i class="fa fa-trash"></i> Hapus</button>
            ';
           })         
           ->rawColumns(['action'])
           ->make(true);
    }

    public function show($id)
    {
        $sl = Station::find($id);
        return Response::json($sl);
    }

    public function store(Request $request)
    {
    	$nama = $request->nama;
    	$slug = $request->slug;
      
      $id    = $request->page_id; 
      $action = $request->action;
      if ($action=='add') {
          $station = new Station();
      } else {
          $station = Station::find($id);
      }
	    $station->nama_stasiun = $nama;
	    $station->stasiun_slug = $slug;
	    
	    $station->save();
      return Response::json($station);
    }

    public function delete($id)
    {
    	$station = Station::find($id)->delete();
      return Response::json($station);
    }
}
