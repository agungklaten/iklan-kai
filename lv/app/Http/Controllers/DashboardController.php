<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Auth;

class DashboardController extends Controller
{
    public function index()
    {
    	$data['menu'] = 'dashboard';
    	$data['page'] = '';
    	return view('page.dashboard',$data);
    }
}
