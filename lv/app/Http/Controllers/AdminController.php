<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use Response;
use App\Models\Admin;
use DB;
use Auth;
use Illuminate\Support\Str;
use App\Models\Station;

class AdminController extends Controller
{
    public function index()
    {
    	$data['menu'] = 'master';
    	$data['page'] = 'admin';    	
      $jml = DB::select('SELECT count(*) as jml from admins where level="0"');
      $data['jml'] = $jml[0]->jml;
      $data['stations'] = Station::get();
    	$data['mobile'] = session('mobile');
    	return view('page.admin',$data);
    }

    public function getData()
    {        
    	$admins = DB::select("SELECT admins.id,name,email,username,nama_stasiun
        from admins 
        inner join stations on stations.id=admins.station_id
        where level = '1'");
    	return Datatables::of($admins)
           ->addColumn('action', function($admin) {
            $leveluser = Auth::user()->level;
            $btn = '<button  class="btn btn-sm btn-info" onclick="opnPage(\''. $admin->id .'\',\''.$admin->id .'\')"><i class="fa fa-edit"></i></button> ';
            if ($leveluser=='0') {
                $btn .='<button class="btn btn-sm btn-danger" onclick="delPage(\''. $admin->id .'\')"><i class="fa fa-trash"></i></button> 
                <button class="btn  btn-sm btn-warning" onclick="changePass(\''. $admin->id .'\')"><i class="fa fa-key"></i></button>';
            }
            return $btn;
           })                      
           ->rawColumns(['action'])
           ->make(true);
    }

    public function show($id)
    {
        $admin = Admin::find($id);
        return Response::json($admin);
    }

    public function store(Request $request)
    {
    	$name = $request->name;
    	$email = $request->email;        
      $id    = $request->page_id;
      $station = $request->station;
      //$code = Str::random(8);        
      $code = '123456';        
      $action = $request->action;
      $check = Admin::where('email',$email)->first();
      if ($check && $action=='add') {
      	$admin['status'] = 'failed';
      	$admin['msg'] = 'Email sudah ada.';
      } else {
        $action = $request->action;
        if ($action=='add') {
            $admin = new Admin();
            $admin->password = bcrypt($code);
        } else {
            $admin = Admin::find($id);
        }
        $admin->name = $name;
        $admin->email = $email;
        $admin->level = '1';     
        $admin->station_id = $station;     
        if ($action=='add') {
           $admin->username = $this->generateUsername($name);
        }
        $admin->save();
        
        $admin->status = 'success';
	    }
        return Response::json($admin);
    }

    public function changepass(Request $request)
    {
        $passwd = $request->passwd;
        $admin = $request->admin;
        $user = Admin::find($admin);
        $user->password = bcrypt($passwd);
        $user->save();
        $data['status'] = 'success';
        return Response::json($data);
    }

    public function delete($id)
    {
    	$admin = Admin::find($id)->delete();
      return Response::json($admin);
    }

    private function generateUsername($name)
    {
        $thename = substr(str_replace(" ","",strtolower($name)),0,8).strtolower(Str::random(4));
        $check = DB::select("SELECT username from admins where username ='$thename' limit 1");
        if ($check) {
            $thename = substr(str_replace(" ","",strtolower($name)),0,8).strtolower(Str::random(4));
        }
        return $thename;
    }
}
