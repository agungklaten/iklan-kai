@extends('layouts.template-top')

@section('content')
<div class="login-box">
  <div class="login-logo">
    <img src="{{ asset('assets/img/logo.png') }}" alt="logo" style="width:90%" />    
    Sign In
  </div>
  <!-- /.login-logo -->
  <div class="login-box-body">    
    <form method="POST" action="{{ route('login') }}">
        @csrf
      <div class="form-group has-feedback">
        <input id="email" type="text" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Email / Username">

        @error('email')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <span class="glyphicon glyphicon-envelope form-control-feedback"></span>
      </div>
      <div class="form-group has-feedback">
        <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password" placeholder="Password">
        @error('password')
            <span class="invalid-feedback" role="alert">
                <strong>{{ $message }}</strong>
            </span>
        @enderror
        <span class="glyphicon glyphicon-lock form-control-feedback"></span>
      </div>
      <div class="row">   
        @if (\Session::has('error'))    
        <div id="alert-msg" class="alert alert-danger alert-dismissible" style="margin-top:10px;display:block;">{!! \Session::get('error') !!}</div>   
        @endif    
      </div>
      <div class="row">
        <div class="col-xs-8">
          @if (Route::has('password.request'))
            <!-- <a class="btn btn-link" href="{{ route('password.request') }}">
                {{ __('Forgot Your Password?') }}
            </a> -->
        @endif
        </div>
        <!-- /.col -->
        <div class="col-xs-4">
          <button type="submit" class="btn btn-primary btn-block btn-flat">Sign In</button>
          <input type="hidden" id="mobile" name="mobile" value="" />
        </div>
        <!-- /.col -->
      </div>
    </form>

    <div>     
    </div>
  </div>
  <!-- /.login-box-body -->
</div><!-- /.login-box -->

@endsection