<div class="user-panel">
    <div class="pull-left image">        
        @if(Auth::user()->avatar)      
        <img id="avatar-left" class="img-circle" src="{{ url('uploads/'.Auth::user()->avatar) }}" alt="User profile picture"> 
        @else 
        <img id="avatar-left" src='{{ asset("assets/img/favicon.png") }}' class="img-circle" alt="User Image" />
        @endif
    </div>
    <div class="pull-left info">
        <p>{{ Auth::user()->name }}</p>
    </div>
</div>

<ul class="sidebar-menu">    
    <li class="treeview @if($menu=='dashboard') active @endif"><a href="{{ url('/home') }}"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a></li>
    <li class="treeview @if($menu=='slider') active @endif"><a href="{{ url('/slider') }}"><i class="fa fa-table"></i> <span>Slider</span></a></li>
    <li class="treeview @if($menu=='jadwal') active @endif"><a href="{{ url('/jadwal') }}"><i class="fa fa-table"></i> <span>Jadwal KA</span></a></li>
    
    
    @if(Auth::user()->level==0)
    <li class="treeview @if($menu=='master') active @endif">
        <a href="#">
            <i class="fa fa-table"></i> <span> Master Data</span><i class="fa fa-angle-left pull-right"></i>
        </a>
        <ul class="treeview-menu">   
            <li class="treeview @if($page=='admin') active @endif"><a href="{{ url('/admin') }}"><i class="fa fa-user-secret"></i> <span>User Admin</span></a></li>
        </ul>
        <ul class="treeview-menu">   
            <li class="treeview @if($page=='station') active @endif"><a href="{{ url('/station') }}"><i class="fa fa-train"></i> <span>Stasiun</span></a></li>
        </ul>
    </li>
    @endif

    <li class="header">&nbsp;</li>
    <li class="treeview @if($menu=='profile') active @endif"><a href="{{ url('/profile') }}"><i class="fa fa-user"></i> <span>Profile</span></a></li>
    <li class="treeview @if($menu=='logout') active @endif"><a href="{{ url('/logout') }}"><i class="fa fa-sign-out"></i> <span>Sign Out</span></a></li>

    


</ul>
