<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <title>KAI</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta http-equiv="refresh" content="7205">
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <link href='{{ asset("assets/img/favicon.ico") }}' rel="shortcut icon">

    <link href='{{ asset("assets/js/plugins/bootstrap/dist/css/bootstrap.min.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/css/style.css") }}?date={{ date("YmdHis") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/js/plugins/font-awesome/css/font-awesome.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/js/plugins/Ionicons/css/ionicons.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/js/plugins/select2/dist/css/select2.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/css/AdminLTE.min.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/css/skins/_all-skins.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/js/plugins/timepicker/bootstrap-timepicker.min.css") }}' rel="stylesheet"  type="text/css" />
    <link href='{{ asset("assets/js/plugins/daterangepicker/daterangepicker.css") }}' rel="stylesheet"  type="text/css" />
    <link href='{{ asset("assets/css/jquery.fancybox.css") }}' rel="stylesheet"  type="text/css" />
    <link href='{{ asset("assets/css/jquery-ui.min.css") }}' rel="stylesheet"  type="text/css" />
    <link href='{{ asset("assets/css/jquery-ui-timepicker-addon.css") }}' rel="stylesheet"  type="text/css" />


    <script src='{{ asset("assets/js/plugins/jquery/dist/jquery.min.js") }}'></script>
    <script src='{{ asset("assets/js/plugins/jquery-ui/jquery-ui.min.js") }}'></script>
    <script src='{{ asset("assets/js/plugins/bootstrap/dist/js/bootstrap.min.js") }}' type="text/javascript"></script>
    <script src='{{ asset("assets/js/plugins/fastclick/lib/fastclick.js") }}'></script>
    <script src='{{ asset("assets/js/app.min.js") }}' type="text/javascript"></script>
    <script src='{{ asset("assets/js/demo.js") }}' type="text/javascript"></script>
    <script src='{{ asset("assets/js/plugins/select2/dist/js/select2.min.js") }}' type="text/javascript"></script>
    <script src='{{ asset("assets/js/bootstrap-filestyle.min.js") }}' type="text/javascript"></script>
    <script src='{{ asset("assets/js/plugins/timepicker/bootstrap-timepicker.min.js") }}'></script>
    <script src='{{ asset("assets/js/jquery.fancybox.js") }}'></script>
    
    
    <script src='{{ asset("assets/js/jquery-ui-timepicker-addon.js") }}'></script>
    <script src='{{ asset("assets/js/number-divider.min.js") }}'></script>
    <script src='{{ asset("assets/js/plugins/slimScroll/jquery.slimscroll.min.js") }}'></script>
    <script src="{{ asset('assets/js/custom.js') }}"></script>
    

    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->

    

  </head>
  <body class="skin-blue sidebar-mini">
    <div class="wrapper">
        <header class="main-header">

        <!-- Logo -->
        <a href='{{ url("/home") }}' class="logo">
          <!-- mini logo for sidebar mini 50x50 pixels -->
          <span class="logo-mini"><b>KAI</b></span>
          <!-- logo for regular state and mobile devices -->
          <span class="logo-lg"><b>KAI</b> Slider</span>
        </a>

        <!-- Header Navbar: style can be found in header.less -->
        <nav class="navbar navbar-static-top" role="navigation">
          <!-- Sidebar toggle button-->
          <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
            <span class="sr-only">Toggle navigation</span>
          </a>
          <!-- Navbar Right Menu -->
          <div class="navbar-custom-menu">
            <ul class="nav navbar-nav">
                            

                <!-- User Account: style can be found in dropdown.less -->
                <li class="dropdown user user-menu">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown">

                    <i class="glyphicon glyphicon-user"></i>
                    <span class="hidden-xs">{{ Auth::user()->name }}</span>
                </a>

                <ul class="dropdown-menu">
                    <!-- User image -->
                    <li class="user-header">
                        @if(Auth::user()->avatar)      
                        <img id="avatar-right" class="img-circle" src="{{ url('uploads/'.Auth::user()->avatar) }}" alt="User profile picture"> 
                        @else 
                        <img id="avatar-right" src='{{ asset("assets/img/favicon.png") }}' class="img-circle" alt="User Image" />
                        @endif
                        <p> {{ Auth::user()->firstname }} </p>
                    </li>

                    <!-- Menu Footer-->
                    <li class="user-footer">
                        <div class="pull-right">
                            <a href='{{ url("/logout") }}' class="btn btn-default btn-flat"><i class="fa fa-sign-out"></i>Sign out</a>
                        </div>
                        <div class="">
                          <a href="{{ url('/profile') }}" class="btn btn-default btn-flat"><i class="fa fa-user"></i> Profile</a>
                      </div>
                    </li>
                </ul>
                
              </li>
            </ul>
          </div>

        </nav>
      </header>

      <!-- Left side column. contains the logo and sidebar -->
      <aside class="main-sidebar">
        <!-- sidebar: style can be found in sidebar.less -->
        <section class="sidebar">
            @yield('navigation')
        </section>
        <!-- /.sidebar -->
      </aside>

      <!-- Content Wrapper. Contains page content -->
      <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
          @yield('contentheader')
        </section>

        <!-- Main content -->
        <section class="content">
            @yield('content')
        </section><!-- /.content -->
      </div><!-- /.content-wrapper -->

      <footer class="main-footer">
        <div class="pull-right hidden-xs">

        </div>
        <strong>Copyright &copy; 2020 - Kereta Api Indonesia</strong>
      </footer>




    </div><!-- ./wrapper -->
    @yield('embedscript')
  <script>
  if (isMobile==true) {
    $("#mobile").val('1');
  } else {
    $("#mobile").val('0');
  }
  </script>
  
  </body>
</html>

<?php
  //set headers to NOT cache a page
  header("Cache-Control: no-cache, no-store, must-revalidate"); //HTTP 1.1
  header("Pragma: no-cache"); //HTTP 1.0
  header("Expires: Sat, 26 Jul 1997 05:00:00 GMT"); // Date in the past
?>