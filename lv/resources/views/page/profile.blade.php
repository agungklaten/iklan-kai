 @extends('layouts.template')

@section('navigation')
    @include('layouts.menu')
@endsection

@section('contentheader')
<h1>
    Profil
    <small>User</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-user"></i> Profil User</a></li>
   
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-4">
        <div class="box box-warning">
            <div class="box-body box-profile">     
               
              <img id="avatar" class="profile-user-img img-responsive" src="{{ asset('assets/img/favicon.png') }}" alt="User profile picture">       

              <h3 class="profile-username text-center" id="profilename">{{ $profile->name }}</h3>              

              <ul class="list-group list-group-unbordered">
              	<li class="list-group-item">
                  <b>Level</b> <span class="pull-right">
                    @if($profile->level==0)
                    Super Admin
                    @elseif($profile->level==1)
                    Admin                     
                    @endif
                    </span>
                </li> 
                <li class="list-group-item">
                  <b>Email</b> <span class="pull-right">{{ $profile->email }}</span>
                </li> 
                              
              </ul>

              <div id="changepass" class="btn btn-primary pull-right btn-50"><b>Ubah Password</b></div> 
              <div id="editprofile" class="btn btn-warning btn-50"><b>Ubah Profil</b></div>
              <input type="hidden" id="username" value="{{ $profile->name }}" />
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="usermodal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-table"></i> Ubah Profil</h4>
            </div>
            <div class="modal-body">
                <form id="frmUser" name="frmUser" novalidate="">

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Nama *</span>
                        <input type="text" class="form-control caps" id="name_user" name="name_user" placeholder="Name" />
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>
                

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Email</span>
                        <input type="email" class="form-control caps" id="email_user" name="email_user" placeholder="Email" readonly />
                        <span class="input-group-addon"><i class="fa  fa-envelope-o"></i></span>
                    </div>
                </div>

                
                
              </form>
              
            </div>
            <div class="modal-footer">                        
                <button type="button" class="btn bg-black" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                <button type="button" class="btn btn-success" id="save_user"><i class="fa fa-save"></i> Simpan</button>               
                <input name="_method" type="hidden" value="PATCH">
                <input type="hidden" name="picture_old" id="picture_old" value="" />
                <input type="hidden" name="tgl" id="tgl" value="" />
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="resetmodal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-table"></i> Ubah Password</h4>
            </div>
            <div class="modal-body">
                <form id="frmReset" name="frmReset" novalidate="">

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Nama *</span>
                        <input type="text" class="form-control caps" id="nameuser" name="nameuser" placeholder="Name" readonly />
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Password Lama *</span>
                        <input type="password" class="form-control" id="oldpasswd" name="oldpasswd" placeholder="Password Lama" />
                        <span class="input-group-addon"><i class="fa  fa-key"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Password Baru *</span>
                        <input type="password" class="form-control" id="passwd" name="passwd" placeholder="Password Baru" />
                        <span class="input-group-addon"><i class="fa  fa-key"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Konfirmasi Password *</span>
                        <input type="password" class="form-control" id="verify" name="verify" placeholder="Verify Password" />
                        <span class="input-group-addon"><i class="fa  fa-key"></i></span>
                    </div>
                </div>

                <div class="form-group">
                  <button id="loading2" class="btn btn-xs btn-primary btn-block" style="display: none">Silahkan tunggu, data sedang diproses</button>
                </div>
                <div id="alert-msg2" class="alert alert-danger alert-dismissible" style="display:none"></div>

              </form>              
            </div>
            <div class="modal-footer">                        
                <button type="button" class="btn bg-black" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                <button type="button" class="btn btn-success" id="saveuser"><i class="fa fa-save"></i> Simpan</button>
                <input name="_method" type="hidden" value="PATCH">
            </div>
        </div>
    </div>
</div>
@endsection

@section('embedscript')
<script>

$(document).ready(function(){    
    $("#save_user").click(function(e) {      
      e.preventDefault();

      if ($("#name_user").val()=='')  {

      } else {

      var  data = new FormData();      
      data.append('_token','{{ csrf_token() }}');
      data.append('name',$("#name_user").val()); 
      data.append('tgl',$("#tgl").val());    
      
      var type = "POST";
      var my_url = "{{ url('/profile/update') }}";

      $.ajax({
        type:type,
        url:my_url,
        data:data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data){
            $("#profilename").html(data.name);   
            $("#username").val(data.name);
            $("#avatar,#avatar-left,#avatar-right,.banner-preview").attr('src',data.filename);            
            $("#usermodal").modal('hide');
            document.location.href = '{{ url('/profile') }}';
        },
        error: function (data) {
            console.log("Error:" + data);
        }
      });
    }
   }); 

  $('.numberonly').keyup(function(){
        this.value = this.value.replace(/[^0-9\.-]/g,'');
    });

    $("#saveuser").click(function(e) {
      e.preventDefault();
      $("#loading2").hide('fast');

      if (($("#oldpasswd").val()=='')  || ($("#passwd").val()=='') || ($("#verify").val()=="") || ($("#passwd").val() != $("#verify").val())) {
        $("#alert-msg2").html('Silahkan isi data lengkap!');
        $("#alert-msg2").show('fast');
        $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
          $(".alert-dismissible").hide('fast');
        });

      } else {
      
      var  data = new FormData();
      data.append('_token','{{ csrf_token() }}');
      data.append('passwd',$("#passwd").val());
      data.append('oldpasswd',$("#oldpasswd").val());
      $("#loading2").show('slow');
      
      var type = "POST";
      var my_url = "{{ url('/profile/changepass') }}";

      $.ajax({
        type:type,
        url:my_url,
        data:data,
        contentType: false,
        processData: false,
        dataType: 'json',
        success: function(data){
            if (data.status=='failed') {
              $("#loading2").hide('fast');
              $("#alert-msg2").html(data.msg);
              $("#alert-msg2").show('fast');
              $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                $(".alert-dismissible").hide('fast');
              });
            } else if (data.status=='success') {
              $("#loading2").hide('fast');
              $("#resetmodal").modal('hide');
            }            
        },
        error: function (data) {
            console.log("Error:" + data);
        }
      });
    }
   });

   $("#editprofile").click(function(){
      
      $("#name_user").val($("#username").val());     
      $("#email_user").val('{{ $profile->email }}');
      $("#save_user").val('update');
      $("#usermodal").modal('show');
   });

   $("#changepass").click(function(){
      $("#nameuser").val('{{ $profile->name }}');
      $("#passwd,#verify").val('');
      $("#resetmodal").modal('show');
   });
});
</script>
@endsection