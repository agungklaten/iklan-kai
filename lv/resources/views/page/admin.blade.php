@extends('layouts.template')

@section('navigation')
    @include('layouts.menu')
@endsection

@section('contentheader')
<h1>
    List
    <small>User Admin</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-user-secret"></i> List User Admin</a></li>
   
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                 <button id="add_page" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Admin</button>
            </div>
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped nowrap">
                <thead>
                <tr>
                  <th>Nama Admin</th>
                  <th>Nama Login</th>
                  <th>Email</th>
                  <th>Station</th>
                  <th nowrap>Action</th>
                </tr>
                </thead>        
              </table>
            </div>
        </div>
    </div>
</div>

<meta name="_token" content="{!! csrf_token() !!}" />

<div class="modal fade" id="pagemodal" tabindex="-1" aria-hidden="true">
<div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-table"></i> Admin</h4>
            </div>
        <div class="modal-body">
            <form id="frmPage" name="frmPage" novalidate="">   
                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Nama Admin *</b></span>
                          <input type="text" class="form-control" id="name" name="name" placeholder="Nama Admin"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Email *</b></span>
                          <input type="text" class="form-control" id="email" name="email" placeholder="Email"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>  

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Stasiun *</b></span>
                          <select class="form-control" id="station" name="station">
                            <option value="0"> Pilih Stasiun </option>
                            @foreach($stations as $s)
                            <option value="{{ $s->id }}">{{ $s->nama_stasiun }}</option>
                            @endforeach
                          </select>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                              
                <div class="form-group">
                  <button id="loading" class="btn btn-xs btn-primary btn-block" style="display: none">Silahkan tunggu, data sedang diproses</button>
                </div>
                <div id="alert-msg" class="alert alert-danger alert-dismissible" style="display:none"></div> 

            </form>
            <div id="del-page">
                    <h4>Kamu akan menghapus data ini.</h4>
                    <h4>Apakah mau lanjut ?</h4>
                </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                Batal
            </button>
            <button type="button" class="btn btn-primary ladda-button" data-style="expand-right"
                    data-size="s" id="save_page"><i class="fa fa-save"></i> Simpan
            </button>
            <button type="button" class="btn btn-danger" id="del_page" value="delete"><i class="fa fa-trash-o"></i> Hapus</button>
            <input type="hidden" name="page_id" id="page_id" value="0"/>
            <input name="_method" type="hidden" value="PATCH">
            <input type="hidden" name="no" id="no" value="0" />
        </div>
    </div>
</div>
</div>

<div class="modal fade" id="resetmodal" tabindex="-1" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-table"></i> Ubah Password</h4>
            </div>
            <div class="modal-body">
                <form id="frmReset" name="frmReset" novalidate="">

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Nama *</span>
                        <input type="text" class="form-control caps" id="nameuser" name="nameuser" placeholder="Name" readonly />
                        <span class="input-group-addon"><i class="fa fa-user"></i></span>
                    </div>
                </div>                

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Password Baru *</span>
                        <input type="password" class="form-control" id="passwd" name="passwd" placeholder="Password Baru" />
                        <span class="input-group-addon"><i class="fa  fa-key"></i></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="input-group input-group-sm">
                        <span class="input-group-addon" style="width:70px">Konfirmasi Password *</span>
                        <input type="password" class="form-control" id="verify" name="verify" placeholder="Verify Password" />
                        <span class="input-group-addon"><i class="fa  fa-key"></i></span>
                    </div>
                </div>

                <div class="form-group">
                  <button id="loading2" class="btn btn-xs btn-primary btn-block" style="display: none">Silahkan tunggu, data sedang diproses</button>
                </div>
                <div id="alert-msg2" class="alert alert-danger alert-dismissible" style="display:none"></div>

              </form>              
            </div>
            <div class="modal-footer">                        
                <button type="button" class="btn bg-black" data-dismiss="modal"><i class="fa fa-times"></i> Batal</button>
                <button type="button" class="btn btn-success" id="saveuser"><i class="fa fa-save"></i> Simpan</button>
                <input name="_method" type="hidden" value="PATCH">
                <input type="hidden" id="adminid" name="adminid" value="0" />
            </div>
        </div>
    </div>
</div>
@endsection

@section('embedscript')
<link href='{{ asset("assets/css/checkbox.min.css") }}' rel="stylesheet" type="text/css" />
<link href='{{ asset("assets/js/plugins/datatables/jquery.dataTables.min.css") }}' rel="stylesheet" type="text/css" />
<link type="text/css" href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" />
<script src="{{ asset('assets/js/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/extensions/Scroller/js/dataTables.scroller.js') }}"></script>
<script>  
  $(document).ready(function(){         
    var t = $('#table1').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('admindata') !!}',
      columns: [
        {data: 'name', name: 'name'},
        {data: 'username', name: 'username'},
        {data: 'email', name: 'email'},
        {data: 'nama_stasiun', name: 'nama_stasiun'},
        {data: 'action',name: 'action',searchable: false}
      ],      
        "iDisplayLength": 5,
        "aLengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
        @if($mobile==1)
        scrollX: true
        @endif   
    });  
    
  });

$('.numberonly').keyup(function(){
        this.value = this.value.replace(/[^0-9\.-]/g,'');
    });

$("#add_page").click(function(){
    $("#save_page").val("add");
    $("#save_page").html("<i class='fa fa-save'></i> Simpan");    
    $("#frmPage").trigger("reset"); 
    $("#frmPage,#save_page").show();
    $("#del_page, #del-page").hide();
    $("#alert-msg").hide('fast');
    $("#pagemodal").modal("show");
});


$("#save_page").click(function(e){    
    e.preventDefault();  
    $("#loading").hide('fast');  
    if ($("#name").val()=="" || $("#email").val()=="" || $("#station").val()=="0")
    {
    	  $("#alert-msg").html('Silahkan isi data lengkap!');
	      $("#alert-msg").show('fast');
	      $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
	        $(".alert-dismissible").hide('fast');
	      });
    } 
    else 
    {
        var  data = new FormData();        
        data.append('_token','{{ csrf_token() }}');
        data.append('name',$("#name").val());
        data.append('email',$("#email").val());        
        data.append('station',$("#station").val());        
        data.append('page_id',$("#page_id").val());
        data.append('action',$("#save_page").val());
        data.append("no",$("#no").val());

        $("#loading").show('slow');
        
        var type = "POST";
        var my_url = "{{ url('/admin') }}";        

        $.ajax({
            type:type,
            url:my_url,
            data:data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data){
                if (data.status=='success') {
                 $("#loading").hide('fast');
                 $("#pagemodal").modal('hide');
                 document.location.href = '{{ url('/admin') }}';
                } else {
                  $("#loading").hide('fast');
                  $("#alert-msg").html(data.msg);
                  $("#alert-msg").show('fast');
                  $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
                    $(".alert-dismissible").hide('fast');
                  });
                }
            },
            error: function(data) {
                console.log("Error:" + data);
            }
        });
    }
});

function changePass(id)
{
   $("#alert-msg2").hide('fast');
   $.get("{{ url('/admin') }}/"+id,function(data){
     $("#frmReset").trigger('reset');
     $("#nameuser").val(data.name);
     $("#adminid").val(data.id);
   });
   $("#resetmodal").modal('show');
}

function opnPage(id,no)
{
	$("#alert-msg").hide('fast');
    $.get("{{ url('/admin') }}/" + id, function (data) {
        $("#frmPage").trigger('reset');
        $("#name").val(data.name);
        $("#email").val(data.email);  
        $("#station").val(data.station_id);      
        
        $("#page_id").val(data.id);        
    });

    $("#no").val(no);
    $("#save_page").html("<i class='fa fa-save'></i> Ubah");
    $("#save_page").val("update");
    $("#del_page, #del-page").hide();
    $("#frmPage, #save_page").show();
    $("#pagemodal").modal('show');
    
}

function delPage(id)
{
  $("#frmPage, #save_page").hide();
  $("#del-page, #del_page").show();
  $("#del_page").attr("onclick","deletePage('"+id+"')");
  $("#pagemodal").modal("show");
} 

function deletePage(id)
{
  $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
     }
  });
  $.ajax({
      type: "DELETE",
      url: "{{ url('/admin') }}/" + id,
      success: function (data) {
        $("#pagemodal").modal("hide");
        document.location.href = '{{ url('/admin') }}';
      },
      error: function (data)  {
        console.log('Error:', data);
      }
  });
}
$("#saveuser").click(function(e) {
  e.preventDefault();
  $("#loading2").hide('fast');

  if (($("#passwd").val()=='') || ($("#verify").val()=="") || ($("#passwd").val() != $("#verify").val())) {
    $("#alert-msg2").html('Silahkan isi data lengkap!');
    $("#alert-msg2").show('fast');
    $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
      $(".alert-dismissible").hide('fast');
    });

  } else {
  
  var  data = new FormData();
  data.append('_token','{{ csrf_token() }}');
  data.append('passwd',$("#passwd").val());
  data.append('admin',$("#adminid").val());
  $("#loading2").show('slow');
  
  var type = "POST";
  var my_url = "{{ url('/changepass') }}";

  $.ajax({
    type:type,
    url:my_url,
    data:data,
    contentType: false,
    processData: false,
    dataType: 'json',
    success: function(data){
        if (data.status=='failed') {
          $("#loading2").hide('fast');
          $("#alert-msg2").html(data.msg);
          $("#alert-msg2").show('fast');
          $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
            $(".alert-dismissible").hide('fast');
          });
        } else if (data.status=='success') {
          $("#loading2").hide('fast');
          $("#resetmodal").modal('hide');
        }            
    },
    error: function (data) {
        console.log("Error:" + data);
    }
  });
}
});
</script>
@endsection