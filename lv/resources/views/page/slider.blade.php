@extends('layouts.template')

@section('navigation')
    @include('layouts.menu')
@endsection

@section('contentheader')
<h1>
    
    <small>Slider</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-table"></i> Slider</a></li>
   
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                 <button id="add_page" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Slider</button>
            </div>
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped nowrap">
                <thead>
                <tr>
                  <th>Title</th>
                  <th>Slug</th>
                  <th>Type</th>
                  <th>Thumbnail</th>
                  <th>Durasi</th>
                  <th>Status</th>
                  <th>Station</th>
                  <th nowrap>Action</th>
                </tr>
                </thead>        
              </table>
            </div>
        </div>
    </div>
</div>

<meta name="_token" content="{!! csrf_token() !!}" />

<div class="modal fade" id="pagemodal" tabindex="-1" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-table"></i> Slider</h4>
            </div>
        <div class="modal-body">
            <form id="frmPage" name="frmPage" novalidate="">   
                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Title *</b></span>
                          <input type="text" class="form-control" id="title" name="title" placeholder="Title"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div> 

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Slug *</b></span>
                          <input type="text" class="form-control" id="slug" name="slug" placeholder="Slug"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Type *</b></span>
                          <select name="tipe" id="tipe" class="form-control">
                          	<option value="image">Image</option>
                          	<option value="video">Video</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div> 

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>File Media *</b></span>
                          <input type="file" class="form-control" id="file" name="file" accept="image/*,video/mp4,video/*"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div> 
                <div id="thumbnail-wrapper" class="form-group" style="display:none;">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Thumbnail *</b></span>
                          <input type="file" class="form-control" id="thumbnail" name="thumbnail" accept="image/*" />
                          <span class="input-group-addon file-upload-desc"> Thumb. size 400 x 225 pixels </span>
                      </div>
                </div>

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Durasi *</b></span>
                          <input type="text" class="form-control numberonly" id="durasi" name="durasi" placeholder="Durasi, dalam detik"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                @if(Auth::user()->station_id==0)
                <div class="form-group">
                   <div class="input-group input-group-sm">
                    <span class="input-group-addon" style="width:125px;text-align:left;"><b>Stasiun *</b></span>
                          <select class="form-control" id="station" name="station">
                            <option value="0">Pilih Stasiun</option>
                            @foreach($stations as $s)
                            <option value="{{ $s->id }}">{{ $s->nama_stasiun }}</option>
                            @endforeach
                          </select>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                @else
                <input type="hidden" name="station" id="station" value="{{ Auth::user()->station_id }}"/>
                @endif
                
                
                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Status *</b></span>
                          <select class="form-control" id="status" name="status">
                            <option value="0">Draft</option>
                            <option value="1">Aktif</option>
                          </select>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                <div class="form-group">
          <button id="loading" class="btn btn-xs btn-primary btn-block" style="display: none">Silahkan tunggu, data sedang diproses</button>
        </div>
            </form>
            <div id="alert-msg" class="alert alert-danger alert-dismissible" style="margin-top:10px;display:none;"></div>
            <div id="del-page">
                    <h4>Kamu akan menghapus data ini.</h4>
                    <h4>Apakah mau lanjut ?</h4>
                </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                Batal
            </button>
            <button type="button" class="btn btn-primary ladda-button" data-style="expand-right"
                    data-size="s" id="save_page"><i class="fa fa-save"></i> Simpan
            </button>
            <button type="button" class="btn btn-danger" id="del_page" value="delete"><i class="fa fa-trash-o"></i> Hapus</button>
            <input type="hidden" name="page_id" id="page_id" value="0"/>
            <input name="_method" type="hidden" value="PATCH">
            <input type="hidden" name="no" id="no" value="0" />
            <input type="hidden" name="media" id="media">
            <input type="hidden" name="thumb" id="thumb">
        </div>
    </div>
</div>
</div>
@endsection

@section('embedscript')
<link href='{{ asset("assets/css/checkbox.min.css") }}' rel="stylesheet" type="text/css" />
<link href='{{ asset("assets/js/plugins/datatables/jquery.dataTables.min.css") }}' rel="stylesheet" type="text/css" />
<link type="text/css" href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" />
<script src="{{ asset('assets/js/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/extensions/Scroller/js/dataTables.scroller.js') }}"></script>

<script>  
  $(document).ready(function(){         
    var t = $('#table1').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('sliderdata') !!}',
      columns: [
        {data: 'title', name: 'title'},
        {data: 'slug', name: 'slug'},
        {data: 'tipe', name: 'tipe'},
        {data: 'thumbnail', name: 'thumbnail'},
        {data: 'durasi', name: 'surasi'},
        {data: 'status', name: 'status'},
        {data: 'nama_stasiun', name: 'nama_stasiun'},
        {data: 'action',name: 'action',searchable: false}
      ],      
        "iDisplayLength": 5,
        "aLengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
        @if($mobile==1)
        scrollX: true
        @endif   
    });  
    
  });
var files = "";
var files2 = "";
$(":file").filestyle({placeholder: "Tidak ada file",text:'Pilih file'});
$("#file").on('change',function (f) {
   files = f.target.files;
 });
$("#thumbnail").on('change',function (f) {
   files2 = f.target.files;
 });
$("#tipe").on('change',function(e){
  var tipe = e.target.value;
  if (tipe=='video') {
    $("#thumbnail-wrapper").fadeIn('fast');
  } else {
    $("#thumbnail-wrapper").fadeOut('fast');
  }
});
$("#title").keyup(function(){
        var Text = $(this).val();
        Text = Text.toLowerCase();
        Text = Text.replace(/[^a-zA-Z0-9]+/g,'-');
        $("#slug").val(Text);        
    });
$("#add_page").click(function(){
    $("#save_page").val("add");
    $("#save_page").html("<i class='fa fa-save'></i> Simpan");    
    $("#frmPage").trigger("reset"); 
    $("#media,#thumb").val('');
    $("#frmPage,#save_page").show();
    $("#del_page, #del-page").hide();
    $("#pagemodal").modal("show");
});

$('.numberonly').keyup(function(){
        this.value = this.value.replace(/[^0-9\.-]/g,'');
    });

$("#save_page").click(function(e){    
    e.preventDefault();  
    
    if ($("#title").val()=="" || $("#slug").val()=="" || ($("#file").val()=="" && $("#media").val()=='')|| $("#durasi").val()=="" || $("#station").val()=='0')
    {
      $("#alert-msg").html('Silahkan isi data lengkap!');
      $("#alert-msg").show('fast');
      $(".alert-dismissible").fadeTo(2000, 500).slideUp(500, function(){
        $(".alert-dismissible").hide('fast');
      });
    } 
    else 
    {
        $("#loading").fadeIn('slow');
        var  data = new FormData();     
        if (files.length > 0) {
          data.append('file',files[0]);
        }   
        if (files2.length > 0) {
          data.append('thumbnail',files2[0]);
        }
        data.append('_token','{{ csrf_token() }}');
        data.append('title',$("#title").val());
        data.append('slug',$("#slug").val());
        data.append('tipe',$("#tipe").val());
        data.append('durasi',$("#durasi").val());
        data.append('status',$("#status").val());
        data.append('page_id',$("#page_id").val());
        data.append('action',$("#save_page").val());
        data.append('media',$("#media").val());
        data.append('thumb',$("#thumb").val());
        data.append('station',$("#station").val());
        
        var type = "POST";
        var my_url = "{{ url('/slider') }}";        

        $.ajax({
            type:type,
            url:my_url,
            data:data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data){
                $("#loading").fadeOut('fast');
                $("#pagemodal").modal('hide');
                document.location.href = '{{ url('/slider') }}';
            },
            error: function(data) {
                console.log("Error:" + data);
            }
        });
    }
});

function opnPage(id,no)
{
    $.get("{{ url('/slider') }}/" + id, function (data) {
        $("#frmPage").trigger('reset');
        $("#title").val(data.title);
        $("#slug").val(data.slug);
        $("#tipe").val(data.tipe);
        if (data.tipe=='video') {
          $("#thumbnail-wrapper").fadeIn('fast');
        }
        $("#status").val(data.status);
        $("#durasi").val(data.durasi);
        $("#media").val(data.namafile);
        $("#thumb").val(data.thumbnail);
        $("#station").val(data.station_id);
        $("#page_id").val(data.id);        
    });

    $("#no").val(no);
    $("#save_page").html("<i class='fa fa-save'></i> Ubah");
    $("#save_page").val("update");
    $("#del_page, #del-page").hide();
    $("#frmPage, #save_page").show();
    $("#pagemodal").modal('show');
    
}

function delPage(id)
{
  $("#frmPage, #save_page").hide();
  $("#del-page, #del_page").show();
  $("#del_page").attr("onclick","deletePage('"+id+"')");
  $("#pagemodal").modal("show");
} 

function deletePage(id)
{
  $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
     }
  });
  $.ajax({
      type: "DELETE",
      url: "{{ url('/slider') }}/" + id,
      success: function (data) {
        $("#pagemodal").modal("hide");
        document.location.href = '{{ url('/slider') }}';
      },
      error: function (data)  {
        console.log('Error:', data);
      }
  });
}
</script>

@endsection