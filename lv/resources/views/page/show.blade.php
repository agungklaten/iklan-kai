<!DOCTYPE html>
<html lang="en">
<head>
    <title>KAI Slider</title>
    <!-- Meta -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="KAI Slider">
    <meta name="author" content="Agung Widada">    
    <link rel="shortcut icon" href="{{ asset('assets/img/favicon.png') }}">  
    <link href='https://fonts.googleapis.com/css?family=Open+Sans:300italic,400italic,600italic,700italic,800italic,400,300,600,700,800' rel='stylesheet' type='text/css'>
    
    <link href='{{ asset("assets/js/plugins/bootstrap/dist/css/bootstrap.min.css") }}' rel="stylesheet" type="text/css" />
    <link href='{{ asset("assets/js/plugins/font-awesome/css/font-awesome.css") }}' rel="stylesheet" type="text/css" />    
    <link id="theme-style" rel="stylesheet" href="{{ asset('assets/css/style.css') }}">
    <link href='{{ asset("assets/css/jquery-ui.min.css") }}' rel="stylesheet"  type="text/css" />

    <script src='{{ asset("assets/js/plugins/jquery/dist/jquery.min.js") }}'></script>
    <script src='{{ asset("assets/js/plugins/jquery-ui/jquery-ui.min.js") }}'></script>
    <script src='{{ asset("assets/js/plugins/bootstrap/dist/js/bootstrap.min.js") }}' type="text/javascript"></script>
    <script src='{{ asset("assets/js/plugins/fastclick/lib/fastclick.js") }}'></script>

    
</head> 
<body>
  <div id="wrapper" class="wrapper">
    @if($media)
    @if($media->tipe=='image')
    <img src="{{ url('/uploads/'.$media->namafile) }}" class="img-media" alt="media">
    @elseif($media->tipe=='video')
    <video id="video" src="{{ url('/uploads/'.$media->namafile) }}" autoplay width="1366" height="768"></video>
    @endif
    @else
     <h1 class="jadwal">Jadwal Kereta Api</h1>
     <table class="table table-striped table-bordered">
        <tbody>
        <tr>
          <th class="jadwal">Nama KA</th>
          <th class="jadwal">No KA</th>
          <th class="jadwal">Jurusan</th>
          <th class="jadwal">Keberangkatan</th>
        </tr>
        @foreach($jadwal as $j)
        <tr>
          <td class="jadwal">{{ $j->nama_ka }}</td>
          <td class="tengah jadwal">{{ $j->no_ka }}</td>
          <td class="jadwal">{{ $j->jurusan }}</td>
          <td class="tengah jadwal">{{ $j->pukul }}</td>
        </tr>
        @endforeach
      </tbody></table>
    @endif
  </div> 

<script>
@if(!$media) 
localStorage.durasi = {{ $jadwal_durasi }};
@else
localStorage.durasi = {{ $media->durasi }};
@endif
var run = setInterval(getmedia,localStorage.durasi*1000);
function getmedia()
{   
    clearInterval(run);
    $.get("{{ url('/get-show/'.$stasiun) }}", function (data) {      
      if (data.tipe=='image') {
        $("#wrapper").html("<img src='"+data.media.filename+"' class='img-media' alt='media'>");        
      } else if (data.tipe=='video') {
        $("#wrapper").html("<video id='video' src='"+data.media.filename+"' autoplay width='1366' height='768'></video>");
      } else if (data.tipe=='jadwal') {
        $("#wrapper").html(data.jadwal);
      }
      localStorage.durasi = data.durasi;  
      run = setInterval(getmedia,(localStorage.durasi*1000));
  });
}

</script>
</body>
</html>