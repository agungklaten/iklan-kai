@extends('layouts.template')

@section('navigation')
    @include('layouts.menu')
@endsection

@section('contentheader')
<h1>
    List
    <small>Jadwal KA</small>
</h1>
<ol class="breadcrumb">
   <li><a href="#"><i class="fa fa-table"></i> List Jadwal KA</a></li>
   
</ol>
@endsection

@section('content')
<div class="row">
    <div class="col-md-12">
        <div class="box box-primary">
            <div class="box-header">
                 <button id="add_page" class="btn btn-sm btn-primary"><i class="fa fa-plus"></i> Tambah Jadwal</button>
            </div>
            <div class="box-body">
              <table id="table1" class="table table-bordered table-striped nowrap">
                <thead>
                <tr>
                  <th>Nama KA</th>
                  <th>No KA</th>
                  <th>Jurusan</th>
                  <th>Pukul</th>
                  <th>Stasiun</th>
                  <th nowrap>Action</th>
                </tr>
                </thead>        
              </table>
            </div>
        </div>
    </div>
</div>

<meta name="_token" content="{!! csrf_token() !!}" />

<div class="modal fade" id="pagemodal" tabindex="-1" aria-hidden="true">
<div class="modal-dialog modal-lg">
    <div class="modal-content">
        <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                <h4 class="modal-title"><i class="fa fa-table"></i> Jadwal KA</h4>
            </div>
        <div class="modal-body">
            <form id="frmPage" name="frmPage" novalidate="">   
                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Nama KA *</b></span>
                          <input type="text" class="form-control" id="namaka" name="namaka" placeholder="Nama KA"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div> 
                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>No KA *</b></span>
                          <input type="text" class="form-control" id="noka" name="noka" placeholder="no KA"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div> 

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Jurusan *</b></span>
                          <input type="text" class="form-control" id="jurusan" name="jurusan" placeholder="Jurusan"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div> 

                <div class="form-group">
                      <div class="input-group input-group-sm">
                          <span class="input-group-addon" style="width:125px;text-align:left;"><b>Pukul *</b></span>
                          <input type="text" class="form-control" id="pukul" name="pukul" placeholder="Pukul"  maxlength="100"/>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                @if(Auth::user()->station_id==0)
                <div class="form-group">
                   <div class="input-group input-group-sm">
                    <span class="input-group-addon" style="width:125px;text-align:left;"><b>Stasiun *</b></span>
                          <select class="form-control" id="station" name="station">
                            <option value="0">Pilih Stasiun</option>
                            @foreach($stations as $s)
                            <option value="{{ $s->id }}">{{ $s->nama_stasiun }}</option>
                            @endforeach
                          </select>
                          <span class="input-group-addon"><i class="fa fa-star"></i></span>
                      </div>
                </div>
                @else
                <input type="hidden" name="station" id="station" value="{{ Auth::user()->station_id }}"/>
                @endif
                
                
                
            </form>
            <div id="del-page">
                    <h4>Kamu akan menghapus data ini.</h4>
                    <h4>Apakah mau lanjut ?</h4>
                </div>
        </div>
        <div class="modal-footer justify-content-between">
            <button type="button" class="btn btn-default" data-dismiss="modal"><i class="fa fa-times"></i>
                Batal
            </button>
            <button type="button" class="btn btn-primary ladda-button" data-style="expand-right"
                    data-size="s" id="save_page"><i class="fa fa-save"></i> Simpan
            </button>
            <button type="button" class="btn btn-danger" id="del_page" value="delete"><i class="fa fa-trash-o"></i> Hapus</button>
            <input type="hidden" name="page_id" id="page_id" value="0"/>
            <input name="_method" type="hidden" value="PATCH">
            <input type="hidden" name="no" id="no" value="0" />
        </div>
    </div>
</div>
</div>
@endsection

@section('embedscript')
<link href='{{ asset("assets/css/checkbox.min.css") }}' rel="stylesheet" type="text/css" />
<link href='{{ asset("assets/js/plugins/datatables/jquery.dataTables.min.css") }}' rel="stylesheet" type="text/css" />
<link type="text/css" href="{{ asset('assets/css/datepicker.css') }}" rel="stylesheet" />
<script src="{{ asset('assets/js/datepicker.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/jquery.dataTables.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/dataTables.bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('assets/js/plugins/datatables/extensions/Scroller/js/dataTables.scroller.js') }}"></script>
<script>  
  $(document).ready(function(){         
    var t = $('#table1').DataTable({
      processing: true,
      serverSide: true,
      ajax: '{!! route('jadwaldata') !!}',
      columns: [
        {data: 'nama_ka', name: 'nama_ka'},
        {data: 'no_ka', name: 'no_ka'},
        {data: 'jurusan', name: 'jurusan'},
        {data: 'pukul', name: 'pukul'},
        {data: 'nama_stasiun', name: 'nama_stasiun'},
        {data: 'action',name: 'action',searchable: false}
      ],  
      "order": [[ 3, 'asc' ]] ,   
        "iDisplayLength": 10,
        "aLengthMenu": [
                    [5, 10, 25, 50, -1],
                    [5, 10, 25, 50, "All"]
                ],
        @if($mobile==1)
        scrollX: true
        @endif   
    });  
    
  });

$("#add_page").click(function(){
    $("#save_page").val("add");
    $("#save_page").html("<i class='fa fa-save'></i> Simpan");    
    $("#frmPage").trigger("reset"); 
    $("#frmPage,#save_page").show();
    $("#del_page, #del-page").hide();
    $("#pagemodal").modal("show");
});


$("#save_page").click(function(e){    
    e.preventDefault();      
    if ($("#namaka").val()=="" || $("#noka").val()=="" || $("#jurusan").val()=="" || $("#pukul").val()=="" || $("#station").val()=='0')
    {

    } 
    else 
    {
        var  data = new FormData();        
        data.append('_token','{{ csrf_token() }}');
        data.append('namaka',$("#namaka").val());
        data.append('noka',$("#noka").val());
        data.append('jurusan',$("#jurusan").val());
        data.append('pukul',$("#pukul").val());
        data.append('page_id',$("#page_id").val());
        data.append('action',$("#save_page").val());
        data.append('station',$("#station").val());
        
        var type = "POST";
        var my_url = "{{ url('/jadwal') }}";        

        $.ajax({
            type:type,
            url:my_url,
            data:data,
            contentType: false,
            processData: false,
            dataType: 'json',
            success: function(data){
                $("#pagemodal").modal('hide');
                document.location.href = '{{ url('/jadwal') }}';
            },
            error: function(data) {
                console.log("Error:" + data);
            }
        });
    }
});

function opnPage(id,no)
{
    $.get("{{ url('/jadwal') }}/" + id, function (data) {
        $("#frmPage").trigger('reset');
        $("#namaka").val(data.nama_ka);
        $("#noka").val(data.no_ka);
        $("#jurusan").val(data.jurusan);
        $("#pukul").val(data.pukul);
        $("#station").val(data.station_id);
        $("#page_id").val(data.id);        
    });

    $("#no").val(no);
    $("#save_page").html("<i class='fa fa-save'></i> Ubah");
    $("#save_page").val("update");
    $("#del_page, #del-page").hide();
    $("#frmPage, #save_page").show();
    $("#pagemodal").modal('show');
    
}

function delPage(id)
{
  $("#frmPage, #save_page").hide();
  $("#del-page, #del_page").show();
  $("#del_page").attr("onclick","deletePage('"+id+"')");
  $("#pagemodal").modal("show");
} 

function deletePage(id)
{
  $.ajaxSetup({
     headers: {
         'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
     }
  });
  $.ajax({
      type: "DELETE",
      url: "{{ url('/jadwal') }}/" + id,
      success: function (data) {
        $("#pagemodal").modal("hide");
        document.location.href = '{{ url('/jadwal') }}';
      },
      error: function (data)  {
        console.log('Error:', data);
      }
  });
}
</script>

@endsection