<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('admins')->insert(['id' => 1, 'name' => 'Super Admin', 'email' => 'agungwidada@gmail.com', 'password' => '$2y$10$ivK72N4g1LIZ9dwVvSrhHe8cBjrQXJeQJeYEoBJD2VNAycaLU4VZi', 'level' => '0', 'username' => 'superadmin', 'created_at' => Carbon::now()->format('Y-m-d H:i:s')]);
    }
}
