<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::pattern('id','[0-9]+');

Auth::routes(['register'=>false]);
Route::group(['middleware'=>'auth'],function(){
	Route::get('/','DashboardController@index');
	Route::get('/home','DashboardController@index')->name('home');

	Route::get('/profile','UserController@profile');
  Route::post('/profile/update','UserController@update');
  Route::post('/profile/changepass','UserController@changepass');

	Route::get('/slider','SliderController@index');
	Route::get('/sliderdata','SliderController@getData')->name('sliderdata');
	Route::get('/slider/{id}','SliderController@show');
	Route::post('/slider','SliderController@store');
	Route::delete('/slider/{id}','SliderController@delete');
	Route::post('/savepicture','SliderController@savepicture');

	Route::get('/admin','AdminController@index');
  Route::get('/admindata','AdminController@getData')->name('admindata');
  Route::get('/admin/{id}','AdminController@show');
  Route::post('/admin','AdminController@store');
  Route::delete('/admin/{id}','AdminController@delete');
  Route::post('/changepass','AdminController@changepass');

  Route::get('/jadwal','JadwalController@index');
	Route::get('/jadwaldata','JadwalController@getData')->name('jadwaldata');
	Route::get('/jadwal/{id}','JadwalController@show');
	Route::post('/jadwal','JadwalController@store');
	Route::delete('/jadwal/{id}','JadwalController@delete');

	Route::get('/station','StationController@index');
	Route::get('/stationdata','StationController@getData')->name('stationdata');
	Route::get('/station/{id}','StationController@show');
	Route::post('/station','StationController@store');
	Route::delete('/station/{id}','StationController@delete');


});

Route::get('/logout','Auth\LoginController@logout');
Route::get('/register','HomeController@index');
//Route::get('/show','SliderController@detail');
Route::get('/get-show/{id}','SliderController@getshow');
Route::get('/show/{slug}','SliderController@showslider');